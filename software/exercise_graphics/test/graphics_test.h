#ifndef GRAPHICS_TEST_H_
#define GRAPHICS_TEST_H_

// The main function to run all graphics tests
int run_graphics_tests(void);

// The graphics tests work by making calls to the graphics function being tested, to draw some
// known shape, and then the pixel values are read and validated. Because we know what shape we
// expect, we can validate that the grahpics function is working properly by checking that the pixels
// it was supposed to set were indeed set the way we expected

// The individual graphics tests. Tests should be separated by component
int write_pixel_test(void);
int h_line_test(void);
int v_line_test(void);
int line_test(void);
int rectangle_test(void);
int triangle_test(void);
int circle_test(void);

#endif

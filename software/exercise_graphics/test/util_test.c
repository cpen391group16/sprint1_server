#include "util_test.h"

#include <stdio.h>
#include "../util/util.h"

int run_util_tests(void) {
	// Since all test functions return 1 if they fail, we bitwise-OR all the results
	// of the test functions to detect if any function failed. If so, we can report
	// that this test suite failed
	int test_failed =
				max_test() |
				min_test() |
				abs_test();

	if(test_failed) {
		printf("util_test failed. Check logs for more info\n");
	}

	return test_failed;
}

int max_test(void) {
	// Test case includes 0 and a positive integer
	int result = max(0, 99);
	if(result != 99) {
		printf("max_test failed. Result was %d, expected %d\n", result, 99);
		return 1;
	}

	// Test case includes negative numbers, and numbers with a large magnitude
	result = max(-12345, -4);
	if(result != -4) {
		printf("max_test failed. Result was %d, expected %d\n", result, -4);
		return 1;
	}

	// Test case includes 0, and numbers that are the same
	result = max(0, 0);
	if(result != 0) {
		printf("max_test failed. Result was %d, expected %d\n", result, 0);
		return 1;
	}

	return 0;
}

int min_test(void) {
	// Test case includes 0
	int result = min(0, 7);
	if(result != 0) {
		printf("min_test failed. Result was %d, expected %d\n", result, 0);
		return 1;
	}

	// Test case includes positive numbers and numbers with large magnitude
	result = min(87, 87263);
	if(result != 87) {
		printf("min_test failed. Result was %d, expected %d\n", result, 87);
		return 1;
	}

	// Test case includes 0 and a negative number
	result = min(-1, 0);
	if(result != -1) {
		printf("min_test failed. Result was %d, expected %d\n", result, -1);
		return 1;
	}

	return 0;
}

int abs_test(void) {
	// Test case covers 0
	int result = abs(0);
	if(result != 0) {
		printf("abs_test failed. Result was %d, expected %d\n", result, 0);
		return 1;
	}

	// Test case covers negative numbers
	result = abs(-12345);
	if(result != 12345) {
		printf("abs_test failed. Result was %d, expected %d\n", result, 12345);
		return 1;
	}

	// Test case covers positive numbers
	result = abs(3);
	if(result != 3) {
		printf("abs_test failed. Result was %d, expected %d\n", result, 3);
		return 1;
	}

	return 0;
}

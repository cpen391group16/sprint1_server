#include "geom_test.h"

#include <stdio.h>
#include "../util/geom.h"

int run_geom_tests(void) {
	// Since all test functions return 1 if they fail, we bitwise-OR all the results
	// of the test functions to detect if any function failed. If so, we can report
	// that this test suite failed
	int test_failed =
			get_larger_y_test() |
			get_smaller_y_test() |
			inside_bounds_test();

	if(test_failed) {
		printf("geom_test failed. Check logs for more info\n");
	}

	return test_failed;
}

int get_larger_y_test(void) {
	Point p1 = {0, 100};
	Point p2 = {50, 0};
	Point p3 = {100, 200};
	Point p4 = {-20, 45};
	Point p5 = {0, -10};

	Point result = get_larger_y(p1, p2);
	if(!points_eq(result, p1)) {
		printf("get_larger_y test failed. Result was point (%d, %d), expected (%d, %d)\n", result.x, result.y, p1.x, p1.y);
		return 1;
	}

	result = get_larger_y(p4, p5);
	if(!points_eq(result, p4)) {
		printf("get_larger_y test failed. Result was point (%d, %d), expected (%d, %d)\n", result.x, result.y, p4.x, p4.y);
		return 1;
	}

	result = get_larger_y(p3, p4);
	if(!points_eq(result, p3)) {
		printf("get_larger_y test failed. Result was point (%d, %d), expected (%d, %d)\n", result.x, result.y, p3.x, p3.y);
		return 1;
	}

	return 0;
}

int get_smaller_y_test(void) {
	Point p1 = {0, 100};
	Point p2 = {50, 0};
	Point p3 = {100, 200};
	Point p4 = {-20, 45};
	Point p5 = {0, -10};

	Point result = get_smaller_y(p1, p2);
	if(!points_eq(result, p2)) {
		printf("get_smaller_y test failed. Result was point (%d, %d), expected (%d, %d)\n", result.x, result.y, p2.x, p2.y);
		return 1;
	}

	result = get_smaller_y(p4, p5);
	if(!points_eq(result, p5)) {
		printf("get_smaller_y test failed. Result was point (%d, %d), expected (%d, %d)\n", result.x, result.y, p5.x, p5.y);
		return 1;
	}

	result = get_smaller_y(p3, p4);
	if(!points_eq(result, p4)) {
		printf("get_smaller_y test failed. Result was point (%d, %d), expected (%d, %d)\n", result.x, result.y, p4.x, p4.y);
		return 1;
	}

	return 0;
}

int inside_bounds_test(void) {
	Point p1 = {100, 300};
	Point p2 = {0, 50};
	Point p3 = {50, 250};
	Bounds b = {50, 400, 0, 400};

	// Covers the case where the point is inside the bounds
	int result = inside_bounds(p1, b);
	if(result != 1) {
		printf("inside_bounds_test failed. Result was %d, expected %d\n", result, 1);
	}

	// Covers the case where the point is outside the bounds
	result = inside_bounds(p2, b);
	if(result != 0) {
		printf("inside_bounds_test failed. Result was %d, expected %d\n", result, 1);
	}

	// This covers the edge case where the point is right on the boundary of the bounds
	result = inside_bounds(p3, b);
	if(result != 1) {
		printf("inside_bounds_test failed. Result was %d, expected %d\n", result, 1);
	}

	return 0;
}

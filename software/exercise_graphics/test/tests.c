#include "tests.h"

#include "graphics_test.h"
#include "util_test.h"
#include "geom_test.h"

// The main function for running the "test suite"
// Other tests can be called and managed from here
int run_tests() {
	// Since all test functions return 1 if they fail, we bitwise-OR all the results
	// of the test functions to detect if any function failed. If so, we can report
	// that this test suite failed
	return run_graphics_tests() |
			run_util_tests() |
			run_geom_tests();
}

/*
 * booth_graphic_interface.h
 */

#include <stdlib.h>

/*
* Checks wether touchscreen has been pressed.
*
* @Return the touch object retrieved
*/
touch get_touch();


/*
* Draws a black wheel with radius of 18 pixels.
*
* @Param x, y coordinates of lowest point of the wheel.
*/
void draw_wheel_r18(int x, int y);


/*
* Moves wheel drawn with draw_wheel_r18() leftward through the screen,
* background colour is white.
*
* @Param x, y coordinates of lowest point of the wheel to be moved.
*/
void move_wheel_r18(int x, int y);


/*
* Moves rectangle at Location leftward through the screen,
* background colour is white.
*
* @Param x, y coordinate of top left corner of rect
* @Param length, height of rectangle
* @Param colour of rectangle
*/
void move_rect(int x, int y, int length, int height, int colour);


/*
* Draws a coaster at specified position
*
* @Param x, y coordinate of top left corner of actual wagon.
*/
void draw_coaster(int position_x, int position_y);


/*
* Moves coaster drawn with draw_coaster() leftward through the screen,
* background colour is white.
*
* @Param x, y coordinate of top left corner of actual wagon.
*/
void move_coaster(int position_x, int position_y);


/*
* Draws the starting frame.
*/
void starting_frame();


/*
* Checks wether start has been pushed.
*
* @Returns 1 if button has been pressed 0 else
*/
int start_button_check();


/*
* Draws the main frame.
*/
void main_frame();


/*
* Writes the current information for ride i into the 
* information box.
*/
void write_info(int i);


/*
* Highlights Ride1.
*/
void ride1_selected();


/*
* Locks in Ride1.
*/
void ride1_locked_in();


/*
* Highlights Ride2.
*/
void ride2_selected();


/*
* Locks in Ride2.
*/
void ride2_locked_in();


/*
* Highlights Ride3.
*/
void ride3_selected();


/*
* Locks in Ride3.
*/
void ride3_locked_in();


/*
* Highlights Ride4.
*/
void ride4_selected();


/*
* Locks in Ride4.
*/
void ride4_locked_in();

/*
* Deselects the currently selected ride/ removes highlight.
*/
void deselect();


/*
* Selects ride i and deselects previous ride.
* 
* @Param i ride being selected.
*/
void coaster_selection(int i);


/*
* Select Button functionality.
* Locks in selected ride.
*/
void select();


/*
* Undoes all selections made so rides can be reselected.
*/
void undo();


/*
* Done button functionality.
* Sends off selected information to server.
*/
void done();


/*
* Switches to map frame.
*/
void view_park();


/*
* Draws the map frame.
*/
void map_frame();


/*
* Checks wether the back button in the map frame has been pressed.
* @Return 1 if true 0 else.
*/
int map_button_check();


/*
* Checks screen for any touch and switches to starting frame if one is detected.
* @Return 1 if touch exists else 0.
*/
int exit_button_check();


/*
* Called in main frame. Checks if there has been a touch,
* if the location fits any button and triggers resulting button functionality.
*/
void main_button_check();


/*
* Main graphics loop that checks all buttons depending on frame and triggers
* all wanted graphics behavior
* Frame has to exist, can be called on any frame, non blocking, should be called in a loop.
*/
void graphics();
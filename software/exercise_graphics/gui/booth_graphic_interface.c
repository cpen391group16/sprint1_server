



/*
 * booth_graphic_interface.c
 */


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "../graphics/graphics_lib.h"
#include "../graphics/Colours.h"
#include "../graphics/font_lib.h"
#include "../devices/lcd_touch.h"
#include "../devices/WiFi.h"
#include "../graphics/image_lib.h"
#include "../graphics/images/ferris_wheel.h"
#include "../graphics/images/pendulum_ride.h"
#include "../graphics/images/swing_ride.h"
#include "../graphics/images/rollercoaster.h"
#include "../graphics/images/map.h"
#include "../web/Ride.h"
#include "../web/Guest.h"

/* selection flags */
int button_pressed = 0;
int ride_1_selected = 0;
int ride_2_selected = 0;
int ride_3_selected = 0;
int ride_4_selected = 0;
int selected = 0;

enum frame {EMPTY, START_FRAME, MAIN_FRAME, MAP_FRAME, END_FRAME}frame;

extern ride_t rides[10];
extern num_rides;
extern guest_t guest[1];

/*
* Checks wether touchscreen has been pressed.
*
* @Return the touch object retrieved
*/
touch get_touch(){
	touch t = touch_screen_status();
	if(t.status == PRESSED && button_pressed == 0){
		button_pressed = 1;
		printf("Button Pressed X: %d Y: %d\n", t.point.x, t.point.y);
		return t;
	}
	else if(t.status == RELEASED && button_pressed == 1){
		printf("Button Released\n");
		button_pressed = 0;
		return t;
	}

	t.status = NOCOMMAND;
	return t;
}

/*
* Draws a black wheel with radius of 18 pixels.
*
* @Param x, y coordinates of lowest point of the wheel.
*/
void draw_wheel_r18(int x, int y){
	int i;
	int k;
	int m = 0;
	int h = 3;
	int j = 5;
	int g = 0;


	for (i = 0; i < 18; i++){


		k = -h;
		while( k <= h){
			write_a_pixel(x + k, y - i, BLACK);
			write_a_pixel(x + k, y - 35 + i, BLACK);
			k++;
		}

		if ( j > 1){
			j--;
			h = h + j;
		} else if ( m > 1){
			m--;

		} else {
			h = h + j;
			g++;
			m = g;
		}
	}
	return;
}

/*
* Moves wheel drawn with draw_wheel_r18() leftward through the screen,
* background colour is white.
*
* @Param x, y coordinates of lowest point of the wheel to be moved.
*/
void move_wheel_r18(int x, int y){
	int i;
	int k;
	int m = 0;
	int h = 3;
	int j = 5;
	int g = 0;


	for (i = 0; i < 18; i++){


		k = -h;

		write_a_pixel(x + k - 1, y - i, BLACK);
		write_a_pixel(x + k - 1, y - 35 + i, BLACK);

		write_a_pixel(x + h, y - i, WHITE);
		write_a_pixel(x + h, y - 35 + i, BLUE);


		if ( j > 1){
			j--;
			h = h + j;
		} else if ( m > 1){
			m--;

		} else {
			h = h + j;
			g++;
			m = g;
		}
	}
	return;

}


/*
* Moves rectangle at Location leftward through the screen,
* background colour is white.
*
* @Param x, y coordinate of top left corner of rect
* @Param length, height of rectangle
* @Param colour of rectangle
*/
void move_rect(int x, int y, int length, int height, int colour){
	v_line (x - 1, y - 1, height + 2, colour);
	v_line (x + length, y - 1, height + 2 , WHITE);
	return;
}


/*
* Draws a coaster at specified position
*
* @Param x, y coordinate of top left corner of actual wagon.
*/
void draw_coaster(int position_x, int position_y){
	int height = 60; 
	int length = 100; 

	rect(position_x + (length/6), position_y, length, height, BLUE, BLUE, 1, 1);

	// passenger 1
	rect(position_x + length/3 + 5, position_y - (height*1)/3 - 3, height/4, (height*1)/3, RED, RED, 1, 1); //body
	rect(position_x + length/3 - height/6, position_y - height/4 - 3, length/20, height/4, DARK_SLATE_GRAY, DARK_SLATE_GRAY, 1, 1); // handle
	circle(position_x + (length/3)+ length/12, position_y - (height*9)/12, height/6, YELLOW, YELLOW,1 ,1); // head
	rect(position_x + (length*4)/5 - height/6, position_y - height, length/20, height/2, WHITE, WHITE, 1, 1);

	// passenger 2
	rect(position_x + (length*4)/5 + 5, position_y - (height*1)/3 - 3, height/4, (height*1)/3, RED, RED, 1, 1); //body
	rect(position_x + (length*4)/5 - height/6, position_y - height/4 - 3, length/30, height/4, DARK_SLATE_GRAY, DARK_SLATE_GRAY, 1, 1); // handle
	circle(position_x + (length*4)/5 + length/12, position_y - (height*9)/12, height/6, YELLOW, YELLOW,1 ,1); // head
	rect(position_x + (length*5)/4 , position_y - height, length/20, height/2, WHITE, WHITE, 1, 1);


	draw_wheel_r18(position_x +(length*5)/12 ,position_y + 18 + height);
	draw_wheel_r18(position_x + (length*6)/7, position_y + 18 + height);

	return;
}


/*
* Moves coaster drawn with draw_coaster() leftward through the screen,
* background colour is white.
*
* @Param x, y coordinate of top left corner of actual wagon.
*/
void move_coaster(int position_x, int position_y){
	int height = 60;
	int length = 100;

	// cart
	move_rect(position_x + (length/6), position_y, length + 1, height, BLUE);
	move_wheel_r18(position_x +(length*5)/12 ,position_y + 18 + height);
	move_wheel_r18(position_x + (length*6)/7, position_y + 18 + height);
	// passenger 1
	move_rect(position_x + length/3 + 5, position_y - (height*1)/3 - 3, height/4 + 1, (height*1)/3, RED); //body
	move_rect(position_x + length/3 - height/6, position_y - height/4 - 3, length/30 + 1, height/4, DARK_SLATE_GRAY); // handle
	circle(position_x + (length/3)+ length/12, position_y - (height*9)/12, height/6, YELLOW, YELLOW,1 ,1); // head
	rect(position_x + (length*4)/5 - height/6, position_y - height, length/20, height/2, WHITE, WHITE, 1, 1);

	// passenger 2
	move_rect(position_x + (length*4)/5 + 5, position_y - (height*1)/3 - 3, height/4 + 1, (height*1)/3, RED); //body
	move_rect(position_x + (length*4)/5 - height/6, position_y - height/4 - 3, length/30 + 1, height/4, DARK_SLATE_GRAY); // handle
	circle(position_x + (length*4)/5 + length/12, position_y - (height*9)/12, height/6, YELLOW, YELLOW,1 ,1); // head
	rect(position_x + (length*5)/4 , position_y - height, length/20, height/2, WHITE, WHITE, 1, 1);

	return;
}

/*
* Draws the starting frame.
*/
void starting_frame(){
	clear(WHITE);
	ride_1_selected = 0;
	ride_2_selected = 0;
	ride_3_selected = 0;
	ride_4_selected = 0;
	selected = 0;

	frame = 1;
	int i;
	for (i = 0; i < 18; i++){
		h_line(0, 325 + i, 800, RED);
	}
	for (i = 0; i < 10; i++){
		h_line(0, 300 + i, 800, RED);
	}

	for (i = 0; i < XRES; i = i + 20){
		rect(i, 309, 5, 15, RED, RED, 1, 1);
	}
	char* string = "START";
	draw_string(string, 305, 200, BLUE, WHITE, HUGE, 0);
	draw_coaster( 600, 222);
	return;
}


/*
* Checks wether start has been pushed.
*
* @Returns 1 if button has been pressed 0 else
*/

int start_button_check(){
	int i;
	button_pressed = 0;
	touch t = get_touch();
	Point p = t.point;
	if(t.status == PRESSED){
		if (p.x >= 300 && p.x <= 500 && p.y >= 200 && p.y <= 280){
			printf("Start Ride Selection Button Hit \n");
			char* string = "START";
			draw_string(string, 305, 200, RED, WHITE, HUGE, 0);
			for (i = 0; i < 800; i++){
				move_coaster(600 - i, 222);
				if (i == 180){
					draw_char(715 - i , 230, BLUE, WHITE, 'E', HUGE, 0);
				} else if ( i == 218){
					draw_char(715 - i , 230, BLUE, WHITE, 'M', HUGE, 0);
				} else if ( i == 256){
					draw_char(715 - i , 230, BLUE, WHITE, 'O', HUGE, 0);
				} else if ( i == 294){
					draw_char(715 - i , 230, BLUE, WHITE, 'C', HUGE, 0);
				} else if ( i == 332){
					draw_char(715 - i , 230, BLUE, WHITE, 'L', HUGE, 0);
				} else if ( i == 370){
					draw_char(715 - i , 230, BLUE, WHITE, 'E', HUGE, 0);
				} else if ( i == 408){
					draw_char(715 - i , 230, BLUE, WHITE, 'W', HUGE, 0);
				}
			}
			main_frame();
			return 1;
		}
	}
	return 0;
}

/*
* Draws the main frame.
*/
void main_frame(){
	frame = 2;
	int i;
	for (i = 1; i < 61; i++){
		rect(680 - 2*i , 120 - 2*i , i*4, i*4, MEDIUM_SPRING_GREEN, MEDIUM_SPRING_GREEN, 1, 1); // info box

		rect(680 - 2*i, 360 - 2*i, i*4, i*4, ORANGE, ORANGE, 1, 1); // finish/undo/select box

		rect( 240 - 4*i, 240 - 4*i, 80 + i*8, i*8, MAGENTA, MAGENTA, 1, 1);
	}
	char* string = "Select Ride";
	draw_string(string, 40, 30, NAVY, MAGENTA, LARGE, 0);


	// Ride information box
	string = "Name:";
	draw_string(string, 580, 10, RED, MEDIUM_SPRING_GREEN, SMALL, 0);
	string = "Ride Type:";
	draw_string(string, 580, 56, RED, MEDIUM_SPRING_GREEN, SMALL, 0);
	string = "Max Speed:";
	draw_string(string, 580, 102, RED, MEDIUM_SPRING_GREEN, SMALL, 0);
	string = "Length";
	draw_string(string, 580, 148, RED, MEDIUM_SPRING_GREEN, SMALL, 0);
	string = "Wait Time";
	draw_string(string, 580, 194, RED, MEDIUM_SPRING_GREEN, SMALL, 0);

	string = "Select";
	draw_string(string, 580, 260, BLUE, ORANGE, LARGE, 0);
	string = "Reset";
	draw_string(string, 580, 340, BLUE, ORANGE, LARGE, 0);
	string = "Finish";
	draw_string(string, 580, 420, BLUE, ORANGE, LARGE, 0);

	rect(360, 10, 190, 70, OLIVE_DRAB, OLIVE_DRAB, 1, 1);
	string = "View Park";
	draw_string(string, 365, 30, BLUE, OLIVE_DRAB, MEDIUM, 0);


	rect(40, 90, 250, 185, LIGHT_SEA_GREEN, LIGHT_SEA_GREEN, -15, 0); //ride 1
	draw_image(55, 105, FERRIS_WHEEL);
	rect(40, 285, 250, 185, LIGHT_SEA_GREEN, LIGHT_SEA_GREEN, -15, 0); // ride 2
	draw_image(55, 300, PENDULUM_RIDE);
	rect(300, 90, 250, 185, LIGHT_SEA_GREEN, LIGHT_SEA_GREEN, -15, 0); // ride 3
	draw_image(315, 105, SWING_RIDE);
	rect(300, 285, 250, 185, LIGHT_SEA_GREEN, LIGHT_SEA_GREEN, -15, 0); // ride 4
	draw_image(315, 300, ROLLERCOASTER);

	return;
}

/*
* Writes the current information for ride i into the 
* information box.
*/
void write_info(int i){
	// Ride Name
	char* string = rides[i].name;
	draw_string(string, 580, 30, RED, MEDIUM_SPRING_GREEN, SMALL, 1);

	// Ride Type
	string = rides[i].ride_type;
	draw_string(string, 580, 76, RED, MEDIUM_SPRING_GREEN, SMALL, 1);

	// Ride Speed
	string = rides[i].max_speed;
	draw_string(string, 580, 122, RED, MEDIUM_SPRING_GREEN, SMALL, 1);

	// Ride Length
	string = rides[i].length;
	draw_string(string, 580, 168, RED, MEDIUM_SPRING_GREEN, SMALL, 1);

	// Wait Time
	char temp[15];
	sprintf(&temp, "%d minutes    ", rides[i].wait_time);
	draw_string(temp, 580, 214, RED, MEDIUM_SPRING_GREEN, SMALL, 1);
	return;
}


/*
* Highlights Ride1.
*/
void ride1_selected(){ 

	rect(40, 90, 250, 185, YELLOW_GREEN, YELLOW_GREEN, -15, 0); 
	write_info(0);
	return;

}

/*
* Locks in Ride1.
*/
void ride1_locked_in(){
	rect(40, 90, 250, 185, BLACK, BLACK, -15, 0); 
	return;
}

/*
* Highlights Ride2.
*/
void ride2_selected(){ 

	rect(40, 285, 250, 185, YELLOW_GREEN, YELLOW_GREEN, -15, 0); 
	write_info(1);
	return;

}

/*
* Locks in Ride2.
*/
void ride2_locked_in(){
	rect(40, 285, 250, 185, BLACK, BLACK, -15, 0); 
	return;
}

/*
* Highlights Ride3.
*/
void ride3_selected(){ 

	rect(300, 90, 250, 185, YELLOW_GREEN, YELLOW_GREEN, -15, 0); 
	write_info(2);
	return;

}

/*
* Locks in Ride3.
*/
void ride3_locked_in(){
	rect(300, 90, 250, 185, BLACK, BLACK, -15, 0); 
	return;
}

/*
* Highlights Ride4.
*/
void ride4_selected(){ 

	rect(300, 285, 250, 185, YELLOW_GREEN, YELLOW_GREEN, -15, 0); 
	write_info(3);
	return;
}

/*
* Locks in Ride4.
*/
void ride4_locked_in(){
	rect(300, 285, 250, 185, BLACK, BLACK, -15, 0); 
	return;
}

/*
* Deselects the currently selected ride/ removes highlight.
*/
void deselect(){

	if (selected == 1) {
		rect(40, 90, 250, 185, LIGHT_SEA_GREEN, LIGHT_SEA_GREEN, -15, 0); //ride 1
	} else if (selected == 2) {
		rect(40, 285, 250, 185, LIGHT_SEA_GREEN, LIGHT_SEA_GREEN, -15, 0); // ride 2
	} else if (selected == 3) {
		rect(300, 90, 250, 185, LIGHT_SEA_GREEN, LIGHT_SEA_GREEN, -15, 0); // ride 3
	} else if (selected == 4) {
		rect(300, 285, 250, 185, LIGHT_SEA_GREEN, LIGHT_SEA_GREEN, -15, 0); // ride 4
	}
	return;
}

/*
* Selects ride i and deselects previous ride.
* 
* @Param i ride being selected.
*/
void coaster_selection(int i){
	if (i == 1 && !ride_1_selected && selected != 1) { //ride1
		ride1_selected();
		deselect();
	}
	if (i == 2 && !ride_2_selected && selected != 2) { //ride2
		ride2_selected();
		deselect ();
	}
	if (i == 3 && !ride_3_selected && selected != 3) { //ride3
		ride3_selected();
		deselect ();
	}
	if (i == 4 && !ride_4_selected && selected != 4) { //ride3
		ride4_selected();
		deselect ();
	}
	selected = i;
	return;
}

/*
* Select Button functionality.
* Locks in selected ride.
*/
void select(){
		 //select
		if (selected == 1){
			ride1_locked_in();
			ride_1_selected = 1;
			selected = 0;
		} else if (selected == 2){
			ride2_locked_in();
			ride_2_selected = 1;
			selected = 0;
		}else if (selected == 3){
			ride3_locked_in();
			ride_3_selected = 1;
			selected = 0;
		}else if (selected == 4){
			ride4_locked_in();
			ride_4_selected = 1;
			selected = 0;
		}

		return;
}

/*
* Undoes all selections made so rides can be reselected.
*/
void undo(){
		if(ride_1_selected == 1){
			selected = 1;
			deselect ();
			ride_1_selected = 0;
		}
		if(ride_2_selected == 1){
			selected = 2;
			deselect ();
			ride_2_selected = 0;
			}
		if(ride_3_selected == 1){
			selected = 3;
			deselect ();
			ride_3_selected = 0;
		}
		if(ride_4_selected == 1){
			selected = 4;
			deselect ();
			ride_4_selected = 0;
		}
		selected = 0;

}

/*
* Done button functionality.
* Sends off selected information to server.
*/
void done(){
		clear(WHITE);
		char* string = "Enjoy the Rides!";
		draw_string(string, 100, 200, RED, WHITE, HUGE, 0);
		frame = 4;

		if(ride_1_selected){
			printf("Send Ride 1 Request\n");
			sendmessage_wifi("tic",new_ticket(1,1));
		}

		if(ride_2_selected){
			printf("Send Ride 2 Request\n");
			sendmessage_wifi("tic",new_ticket(1,2));
		}

		if(ride_3_selected){
			printf("Send Ride 3 Request\n");
			sendmessage_wifi("tic",new_ticket(1,3));
		}

		if(ride_4_selected){
			printf("Send Ride 4 Request\n");
			sendmessage_wifi("tic",new_ticket(1,4));
		}

		return;
}

/*
* Switches to map frame.
*/
void view_park(){
	map_frame();
	return;
}

/*
* Draws the map frame.
*/
void map_frame(){
	frame = 3;
	clear(WHITE);

	draw_image(0, 0, MAP);

	int i;
	for(i = 0; i < num_rides;i++){
		marker(convert_coord_to_x_y(rides[i].lat,rides[i].lon),rides[i].name, 2);
	}

	marker(convert_coord_to_x_y(guest->lat,guest->lon),"",4);
	
	// Legend
	rect(0,  400, 160, 80, WHITE, WHITE, 1, 1);
	legend_marker((Point){40,420}, "Ride", RED);
	legend_marker((Point){40,450}, "Guests", BLUE);

	// Back Button
	rect( 0,0 , 130, 80, TOMATO, TOMATO, 1, 1);
	char* string = "Back";
	draw_string(string, 40, 30, BLUE, ORANGE, MEDIUM, 0);

	return;
}

/*
* Checks wether the back button in the map frame has been pressed.
* @Return 1 if true 0 else.
*/
int map_button_check(){
	touch t = get_touch();
	Point p = t.point;
	if (t.status == PRESSED){
		if (p.x >= 0 && p.x <= 130 && p.y >= 0 && p.y <= 80){
			main_frame();
			return 1;
		} else {
			// update
			printf("Update Map");
		}
	}
	return 0;
}

/*
* Checks screen for any touch and switches to starting frame if one is detected.
* @Return 1 if touch exists else 0.
*/
int exit_button_check(){
	touch t = get_touch();
	Point p = t.point;
	if (t.status == PRESSED){
		if (p.x >= 0 && p.x <= 800 && p.y >= 0 && p.y <= 480){
			starting_frame();
			return 1;
		}
	}
	return 0;
}


/*
* Called in main frame. Checks if there has been a touch,
* if the location fits any button and triggers resulting button functionality.
*/
void main_button_check(){
	touch t = get_touch();
	Point p = t.point;

	if (t.status == PRESSED){
		if ( p.x <= 550){ // Ride Buttons + View Park
			if ( p.x <= 290){ // 1st Column

				if ( p.y >= 285){ // 2nd Row
					coaster_selection(2);
				} else if ( p.y >= 90){ // First Row
					coaster_selection(1);
				}
			} else { // 2nd Column

				if ( p.y >= 285 ){  // 2nd Row
					coaster_selection(4);
				} else if ( p.y >= 90){ // 1st Row
					coaster_selection(3);
				} else if ( p.x >= 360){ // View Park Button
					view_park();
				}
			}

		} else { // Side Bar

			if ( p.y >= 400){ // Finish
				done();
			} else if ( p.y >= 320){ // Reset
				undo();
			} else if (p.y >= 240){ // Select
				select();
			}
		}
	}
	return;
}

/*
* Main graphics loop that checks all buttons depending on frame and triggers
* all wanted graphics behavior
* Frame has to exist, can be called on any frame, non blocking, should be called in a loop.
*/
void graphics(){
	if (frame == START_FRAME){
		start_button_check();
	} else if (frame == MAIN_FRAME){
		main_button_check();
	} else if (frame == MAP_FRAME){
		map_button_check();
	} else if (frame == END_FRAME){
		exit_button_check();
	}
	return;
}
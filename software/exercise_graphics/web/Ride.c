/*
 * Ride.c
 *
 * This file contains the implementation of the prototypes in Ride.h
 *
 * @author Timothy Leung
 */
#include <string.h>
#include <stdio.h>
#include "Ride.h"
#include "../util/cJSON.h"
#include "../util/util.h"
#include "../devices/WiFi.h"

/* Header that prefixes the response from ESP8266 */
#define RIDES_HEADER "rides"

/*
 * @brief Blocking function that sends a wifi command to retrieve the rides
 *
 * This function will send the relevant serial command to the ESP8266 to retrieve a JSON formatted string of all the rides.
 * If the serial response from the ESP8266 is invalid (ie. parsing error), it will wait 1 second before sending the serial command again.
 * When the response is successfully received and parsed into the array of ride_t, the function will return.
 *
 * @param rides output array to store rides
 * @param maximum number of rides (size allocated to rides)
 * @param num_rides modifies location with number of rides returned
 */
void block_and_get_rides(ride_t *rides ,int max_rides, int* num_rides)
{
	char wifi_buffer[1000];
	enum status{SENT, RESEND, VALID}status = RESEND;

	printf("Getting Rides\n");

	while(1){
		if(status == RESEND){
			usleep(1000000);
			sendandgetresponse_wifi(RIDES_HEADER, "", wifi_buffer);
			status = SENT;
		}


		if(strstr(wifi_buffer,RIDES_HEADER) && parse_all_rides(wifi_buffer+strlen(RIDES_HEADER), max_rides, num_rides, rides)){
			/* Sucessfully parsed all rides from JSON string into rides structure, return*/
			break;
		}
		else{
			/* Recieved message was not parsed sucessfully */
			status = RESEND;
		}
	}

	/* Print Rides */
	int i;
	for(i = 0; i< *num_rides;i++){
		printf("Ride: %s Wait: %d Lat: %s Lon: %s\n", rides[i].name,rides[i].wait_time,rides[i].lat,rides[i].lon);
	}
}

/*
 * Parses a given /rides/ json string for all rides
 *
 * @param json - The json string to parse
 * @param max_rides - The size of the local rides array
 * @paramreturn ride_t* local_rides - pointer to first ride_t element of an array.
 *
 * return status 0 if parse failed 1 if sucessful
 */
int parse_all_rides(char *json, int max_rides, int* num_rides, ride_t *local_rides){

	int status = 0;
	cJSON *rides = cJSON_Parse(json);

	if (rides == NULL){
		const char *error_ptr = cJSON_GetErrorPtr();
		if (error_ptr != NULL){
			printf("Error before: %s\n", error_ptr);
		}
		status = 0;
		goto end;
	}

	/* Parse each object in JSON array into a ride_t struct for each ride */
	*num_rides = 0;
	cJSON *ride = rides ? rides->child : 0;
	while (ride)
	{
		cJSON *name = cJSON_GetObjectItem(ride, "name");
		cJSON *wait_time = cJSON_GetObjectItemCaseSensitive(ride, "wait_time");
		cJSON *lat = cJSON_GetObjectItemCaseSensitive(ride, "lat");
		cJSON *lon =  cJSON_GetObjectItemCaseSensitive(ride, "lon");
		cJSON *length = cJSON_GetObjectItemCaseSensitive(ride, "length");
		cJSON *max_speed =  cJSON_GetObjectItemCaseSensitive(ride, "max_speed");
		cJSON *ride_type = cJSON_GetObjectItemCaseSensitive(ride, "ride_type");


		ride_t *curr_ride = &local_rides[*num_rides];

		/* Copy string from parsed json object into our structure */
		strcpy(curr_ride->name,name->valuestring);
		curr_ride->wait_time = wait_time->valueint;
		strcpy(curr_ride->lat,lat->valuestring);
		strcpy(curr_ride->lon,lon->valuestring);
		strcpy(curr_ride->length,length->valuestring);
		strcpy(curr_ride->max_speed,max_speed->valuestring);
		strcpy(curr_ride->ride_type,ride_type->valuestring);


		/* Add Trailing Spaces as the UI needs to erase previous string*/
		add_trailing_spaces_to_14(curr_ride->name);
		add_trailing_spaces_to_14(curr_ride->ride_type);
		add_trailing_spaces_to_14(curr_ride->max_speed);
		add_trailing_spaces_to_14(curr_ride->length);

		(*num_rides) ++;
		if((*num_rides) == max_rides)
			break;

		ride=ride->next;
	}
	status = 1;

	end:
	cJSON_Delete(rides);
	return status;
}



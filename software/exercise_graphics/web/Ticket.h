/*
 * Ticket.h
 *
 * This library contains the prototypes and structs for anything related to a Ticket
 *
 * @author Timothy Leung
 */

#ifndef TICKET_H_
#define TICKET_H_

/*
 * Ticket Structure for storing information about a given ticket
 */
typedef struct {
   char  start_time[15];
   char  end_time[15];
   char  ride_name[20];
   char  ride_lat[20];
   char  ride_lon[20];
} ticket_t;


/*
 *  This method return a JSON-formatted string for a given guest and ride. The JSON
 *  object is formatted for a POST call to /tickets/.
 *
 *  @param guest_id The guest id that this ticket request should be created for
 *  @param guest_id The ride id that this ticke request should be created for
 *
 *  @return JSON-formatted string of a new ticket request
 */
char* new_ticket(int guest_id, int ride_id);

#endif /* TICKET_H_ */

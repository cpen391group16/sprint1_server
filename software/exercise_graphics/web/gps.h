/*
 * GPS.h
 *
 * Function related to parsing GPS information
 *
 *  Author: Timothy Leung
 */

#ifndef GPS_H_
#define GPS_H_

/*
 * Parse guest gps json response into guest struct
 *
 * @param json string to parse
 * @param guest guest_t to parse to
 */
int parse_gps_loc(char* json, guest_t *guest);

#endif /* GPS_H */

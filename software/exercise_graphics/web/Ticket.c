/*
 * Ticket.c
 *
 *  Created on: Feb 10, 2018
 *      Author: timot
 */
#include "Ticket.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "../util/cJSON.h"


/*
 *  This method return a JSON-formatted string for a given guest and ride. The JSON
 *  object is formatted for a POST call to /tickets/.
 *
 *  @param guest_id The guest id that this ticket request should be created for
 *  @param guest_id The ride id that this ticke request should be created for
 *
 *  @return JSON-formatted string of a new ticket request
 */
char* new_ticket(int guest_id, int ride_id)
{
	char *string = NULL;

	// Create GPS Object
	cJSON *new_ticket_request = cJSON_CreateObject();
	if ( new_ticket_request == NULL){
		goto end;
	}

	// Add format to signal to rails server that our request is JSON format
	if (cJSON_AddStringToObject(new_ticket_request, "format", "json") == NULL){
		goto end;
	}

	// Add Guest Array
	cJSON *ticket = cJSON_CreateObject();
	if (ticket == NULL){
		goto end;
	}
	// Add ride_id and guest_id to ticket object
	if (cJSON_AddNumberToObject(ticket, "ride_id", ride_id) == NULL){
		goto end;
	}
	if (cJSON_AddNumberToObject(ticket, "guest_id", guest_id) == NULL){
		goto end;
	}
	cJSON_AddItemToObject(new_ticket_request, "ticket", ticket);


	/* Uncomment if you want to see formatted JSON string (with CRLFs)*/
	string = cJSON_Print(new_ticket_request);
	//printf("%s",string);


	string = cJSON_PrintUnformatted(new_ticket_request);
	//printf("%s\n",string);

	end:
	cJSON_Delete(new_ticket_request);
	return string;
}




/*
 * Ride.h
 *
 * This library contains the prototypes and structs for anything related to a Ride
 *
 * @author Timothy Leung
 */

#ifndef RIDE_H_
#define RIDE_H_

/* Ride Structure */
typedef struct{
   char name[15];
   char lat[15];
   char lon[15];
   int wait_time;
   char length[15];
   char max_speed[15];
   char ride_type[15];
} ride_t;

/*
 * @brief Blocking function that sends a wifi command to retrieve the rides
 *
 * This function will send the relevant serial command to the ESP8266 to retrieve a JSON formatted string of all the rides.
 * If the serial response from the ESP8266 is invalid (ie. parsing error), it will wait 1 second before sending the serial command again.
 * When the response is successfully received and parsed into the array of ride_t, the function will return.
 *
 * @param rides output array to store rides
 * @param maximum number of rides (size allocated to rides)
 * @param num_rides modifies location with number of rides returned
 */
void block_and_get_rides(ride_t *rides ,int max_rides, int* num_rides);

/*
 * Parses a given /rides/ json string for all rides
 *
 * @param json - The json string to parse
 * @param max_rides - The size of the local rides array
 * @paramreturn ride_t* local_rides - pointer to first ride_t element of an array.
 *
 * return status 0 if parse failed 1 if sucessful
 */
int parse_all_rides(char *json,int max_rides, int *num_rides, ride_t *local_rides);

#endif /* RIDE_H_ */

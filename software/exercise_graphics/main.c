#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "graphics/graphics_lib.h"
#include "graphics/Colours.h"
#include "devices/lcd_touch.h"
#include "devices/WiFi.h"
#include "gui/booth_graphic_interface.h"
#include "web/Ride.h"
#include "web/Guest.h"
#include "sys/alt_alarm.h"

// This line guarantees that during normal operation, the macro for running tests is not defined
#undef RUN_TESTS

#define ALL_RIDES "rides"
#define PUSH_BUTTONS ((~(*(volatile unsigned char *)(0x80002060)))& 0x7)

// If this line is uncommented, the test suite will be compiled and run
//#define RUN_TESTS

#ifdef RUN_TESTS
#include "test/tests.h"

int main() {
	printf("Welcome to NIOS II\n");
	printf("Running test suite...\n");

	// Run the testsuite
	if(!run_tests()) {
		printf("Congratulations! All tests pass.\n");
	}

	return 0;
}
#else

/* Data */
ride_t rides[10];
int max_rides = 10;
int num_rides = 0;
guest_t guest[1];

void init()
{
	/* Initialize devices */
	init_touch();
	printf("Init Touchscreen\n");

	init_wifi();
	printf("Init WiFi\n");
	sendstring_wifi("ClearBuffer");
	clear_buffer();
	clear_buffer();
	clear_buffer();

	load_colour_palette(NORMAL);
	printf("Init Palette\n");

	/* Display starting frame */
	starting_frame();

	/* Retrieve Data from Webserver */
	block_and_get_rides(rides , max_rides, &num_rides);
	block_and_get_guest_information(guest);
}

int main()
{
	/* Initialize Method */
	init();

	printf("Starting main loop\n");
	while(1) {
		graphics();
	}
	return 0;
}

#endif

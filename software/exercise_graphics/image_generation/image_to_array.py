import cv2
import math
import os
import color_palette

# This script acts as a custom-written version of TheDotFactory. I wrote it to give myself more flexibility and
# control over what was generated, and the parameters I could use
#
# This script takes in a list of image files and desired sizes, and auto-generated .c and .h files containing
# arrays corresponding to the color data of the image mapped to the given colorpalette. These arrays can then be
# used to draw the image on the LCD display in color.

# Where to output the auto-generated .c and .h files.
OUTPUT_FILEPATH = "../graphics/images/"

# The x and y resolution of the LCD Touch Display
XRES = 800
YRES = 480

# The list of files to generate. The list contains triples of 'filename/path', width, height
# If None is used for width or height the original dimensions of the image are preserved
inputFiles = [('map.png', XRES, YRES),
             ]

for input_file in inputFiles:
    inputFilepath = input_file[0]
    inputWidth = input_file[1]
    inputHeight = input_file[2]

    filename_w_ext = os.path.basename(inputFilepath)
    filename, file_extension = os.path.splitext(filename_w_ext)

    # Read the image as color and convert to RGB format
    img = cv2.imread(inputFilepath, 1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # Set the width and height to resize the image if necessary. Because opencv deals with images in terms of rows
    # and colums rather than x and y, the coordinates are reversed
    width = img.shape[1] if inputWidth is None else inputWidth
    height = img.shape[0] if inputHeight is None else inputHeight
    total_pixels = img.size

    img = cv2.resize(img, (width, height))

    print("image size is {} by {}, {} total pixels".format(width, height, total_pixels))

    # Because the images are indexes by row and column rather than x, y we need to iterate x through height, and y through width
    for x in range(height):
        for y in range(width):
            # pixels colors are stored in BGR format
            r = img.item(x, y, 0)
            g = img.item(x, y, 1)
            b = img.item(x, y, 2)

            # Create a hex RGB value in the form 0x00RRGGBB
            h = (r << 16) | (g << 8) | b
            # Get the new hex value that matches the color palette
            # print("Turned ({}, {}, {}) into {}".format(r, g, b, hex(h)))
            palette_index = color_palette.get_closest_val_in_palette_index(h)

            # Store the mapped color entry into the blue component of the image. This saves us from
            # creating another large array
            img.itemset((x, y, 0), palette_index)

    # Write the image arrays to files
    output_source_file =  OUTPUT_FILEPATH + filename + ".c"
    output_header_file =  OUTPUT_FILEPATH + filename + ".h"

    # Create the auto-generated header file
    with open(output_header_file, 'w') as f:
        # Write the header guards
        f.write("// This is an auto-generated file\n")
        f.write("#ifndef {}_H_\n#define {}_H_\n\n".format(filename.upper(), filename.upper()))

        # Write the includes
        f.write('#include "../image_lib.h"\n\n')

        # Write the extern Image declaration for this image
        f.write("extern const Image {};\n\n".format(filename.upper()))

        # Write the end of the header guard
        f.write("#endif\n")

    # Create the auto-generated c file
    with open(output_source_file, 'w') as f:
        # Write the include for the header
        f.write('#include "{}.h"\n\n'.format(filename))

        # Write the array containing the image data
        f.write("const unsigned char {}[] = {}\n".format(filename, '{'))
        for x in range(height):
            for y in range(width):
                f.write("{}, ".format(img.item(x, y, 0)))
            f.write("\n")
        f.write("};\n\n")

        # Write the extern Image instantiation for this image
        f.write("const Image {} = {}{}, {}, {}, {}{};\n".format(filename.upper(), '{', width, height, filename, "NORMAL", '}'))

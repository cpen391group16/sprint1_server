/*
 * fontlib.c
 *
 *  Created on: Jan 30, 2018
 *      Author: Mathew
 */

#include <stdio.h>
#include <assert.h>
#include "font_lib.h"
#include "../graphics/graphics_lib.h"

// Set the font sizes
const Font TINY = {5, 7, 1, 0, 0};
const Font SMALL = {10, 14, 3, 0, 0};
const Font MEDIUM = {16, 27, 5, 54, (unsigned char *) &Font16x27};
const Font LARGE = {22, 40, 7, 120, (unsigned char *) &Font22x40};
const Font HUGE = {32, 59, 9, 236, (unsigned char *) &Font38x59};

int font_eq(Font f1, Font f2) {
	return f1.width == f2.width &&
		   f1.height == f2.height &&
		   f1.buffer == f2.buffer &&
		   f1.elementsPerChar == f2.elementsPerChar &&
		   f1.font_array == f1.font_array;
}

/*
 * Draws a char with dimensions 5x7. This is a provided function
 */
void DrawChar5x7(int x, int y, int fontcolour, int backgroundcolour, int c, int Erase)
{
	// using register variables (as opposed to stack based ones) may make execution faster
	// depends on compiler and CPU
	register int row, column, theX = x, theY = y ;
	register int pixels ;
	register char theColour = fontcolour  ;
	register int BitMask, theC = c ;

	// if x,y coord off edge of screen don't bother
    if(((short)(x) > (short)(XRES-1)) || ((short)(y) > (short)(YRES-1)))
        return ;

    // if printable character subtract hex 20
	if(((short)(theC) >= (short)(' ')) && ((short)(theC) <= (short)('~'))) {
		theC = theC - 0x20 ;
		for(row = 0; (char)(row) < (char)(7); row ++)	{
			// get the bit pattern for row 0 of the character from the software font
			pixels = Font5x7[theC][row] ;
			BitMask = 16 ;

			for(column = 0; (char)(column) < (char)(5); column ++)	{
				// if a pixel in the character display it
				if((pixels & BitMask))
					write_a_pixel(theX+column, theY+row, theColour) ;
				else {
					if(Erase == 1)
						// if pixel is part of background (not part of character)
						// erase the background to value of variable BackGroundColour
						write_a_pixel(theX+column, theY+row, backgroundcolour) ;
				}
				BitMask = BitMask >> 1 ;
			}
		}
	}
}

/*
 * Draws a char with dimensions 10x14. This is a provided function
 */
void DrawChar10x14(int x, int y, int colour, int backgroundcolour, int c, int Erase)
{
	register int 	row,
					column,
					theX = x,
					theY = y ;
	register int 	pixels ;
	register char 	theColour = colour  ;
	register int 	BitMask,
					theCharacter = c,
					theRow, theColumn;

    if(((short)(x) > (short)(XRES-1)) || ((short)(y) > (short)(YRES-1)))  // if start off edge of screen don't bother
        return ;

	if(((short)(theCharacter) >= (short)(' ')) && ((short)(theCharacter) <= (short)('~'))) {			// if printable character
		theCharacter -= 0x20 ;																			// subtract hex 20 to get index of first printable character (the space character)
		theRow = 14;
		theColumn = 10;

		for(row = 0; row < theRow ; row ++)	{
			pixels = Font10x14[theCharacter][row] ;		     								// get the pixels for row 0 of the character to be displayed
			BitMask = 512 ;							   											// set of hex 200 i.e. bit 7-0 = 0010 0000 0000
			for(column = 0; column < theColumn;   )  	{
				if((pixels & BitMask))														// if valid pixel, then write it
					write_a_pixel(theX+column, theY+row, theColour) ;
				else {																		// if not a valid pixel, do we erase or leave it along (no erase)
					if(Erase == 1)
						write_a_pixel(theX+column, theY+row, backgroundcolour) ;
					// else leave it alone
				}
					column ++ ;
				BitMask = BitMask >> 1 ;
			}
		}
	}
}

/*
 * Draws a using the given font
 */
void DrawCustomChar(int x, int y, int colour, int backgroundcolour, int c, int Erase, Font font)
{
	// How many elements in the array are used for each char.
	register int elementsPerChar = font.elementsPerChar;
	// The starting index in the array for the character
	register unsigned int baseIndex = 0;
	// The pixels for each element of the array, representing the pixels of the char
	register unsigned char pixels = 0;
	// The bitmask for iterating through pixels
	register unsigned char bitMask = 0;
	// The pointer to the array that stores the set of chars for this font
	register unsigned char* font_array = font.font_array;
	// Loop variables
	register unsigned int i = 0, w = 0, row = 0;

	// if x,y coord off edge of screen don't bother
    if(((short)(x) > (short)(XRES-1)) || ((short)(y) > (short)(YRES-1)))
        return;

	// if printable character subtract hex 20
	if(((short)(c) >= (short)(' ')) && ((short)(c) <= (short)('~'))) {
		// The index of the first element in the font array for this char
		baseIndex = (c - 0x20) * elementsPerChar;

		// Loop through the array elements for a single character
		i = baseIndex;
		pixels = font_array[i];
		while(i < baseIndex + elementsPerChar) {
			for(w = font.width; w > 0; w--) {
				bitMask = 1 << ((w - 1) % 8);

				if(pixels & bitMask) {
					write_a_pixel(x + font.width - w, y + row, colour) ;
				}else if (Erase == 1){
					write_a_pixel(x + font.width - w, y + row, backgroundcolour) ;
				}

				// After printing a full byte of pixels, move to the next byte in the array
				if((w - 1) % 8 == 0){
					pixels = font_array[++i];
				}
			}
			row++;
		}
	}
}

/*
 * A universal function for drawing characters with a given font
 */
void draw_char(int x, int y, int fontcolour, int backgroundcolour, int c, Font font, int Erase)
{
	if(font_eq(font, TINY))
		DrawChar5x7(x, y, fontcolour, backgroundcolour, c, Erase);
	else if(font_eq(font, SMALL))
		DrawChar10x14(x, y, fontcolour, backgroundcolour, c, Erase);
	else {
		DrawCustomChar(x, y, fontcolour, backgroundcolour, c, Erase, font);
	}
}

/*
 * Draws a non-null string starting at the given coordinates with the given font
 */
void draw_string(char* string, int x, int y, int fontcolour, int backgroundcolour, Font font, int Erase) {
	assert(string != NULL);
	int px = x, py = y;
	int index = 0;
	char c = string[index];

	// To draw the string, draw each character and separate it by the buffer specified by the font
	while(c != '\0') {
		draw_char(px, py, fontcolour, backgroundcolour, c, font, Erase);
		px += font.width + font.buffer;
		c = string[++index];
	}
}

/*
 * Returns the total length that a string with a given font will take up in pixels
 */
int get_string_pixel_width(char* string, Font font) {
	assert(string != NULL);
	int width = 0;

	int i = 0;
	char c = string[0];
	while(c != '\0') {
		width += font.width + font.buffer;
		c = string[++i];
	}

	width -= font.buffer; // Remove the extra buffer added
	return width;
}

/*
 * Returns the total height a string with a given font will take up in pixels
 */
int get_string_pixel_height(char* string, Font font) {
	return font.height;
}

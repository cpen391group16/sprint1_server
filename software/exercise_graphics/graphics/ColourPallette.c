/*
 * ColourPalletteData.c
 *
 *  Created on0xApr 12, 2015
 *      Author0xPaul
 */

// see --http://www.rapidtables.com/web/color/RGB_Color.htm for example colours
//
// Constants for each pallette number using the name for the colour can be found in the header file "Colours.h"
//
// this data represents the 24 bit RGB values of 256 colours. DE2 can display all 256 simultaneously, but DE1 can only display 64 at any one time.
// It should be setup in hardware in a ROM file in Quartus
// but the software ('C' code) version of the data is also given below and can be used as a reference to the original data in graphics chip ROM
//
// You should program the colour pallette (DE2 = 256, DE1 = 64) at the start, i.e in main() by calling ProgramPallette(BLACK, ColourPalletteData[0])
// for each colour and programming the colour(s) you want, 1 RGB value per pallette. BLACK is enumerated as 0 in Colours.h header file
//
// You should include this as part of the project so it gets compiled, but if you want to reference it in multiple source file
// you should put an "extern" declaration in those source files e.g. extern const unsigned int ColourPalletteData[256]

const unsigned int ColourPalletteData[64] = {
0x00000000, // Black
0x00FFFFFF, // White
0x00FF0000, // Red
0x00f7c587, // Brown
0x000000FF, // Blue
0x00FFFF00, // Yellow
0x0000FFFF, // Cyan
0x00FF00FF, // Magenta
0x00C0C0C0, // Silver
0x00808080, // Gray
0x00800000, // Maroon
0x00808000, // Olive
0x00008000, // DarkGreen
0x00800080, // Purple
0x00008080, // Teal
0x00000080, // Navy
0x008B0000, // Dark Red
0x00A52A2A, // Brown
0x00B22222, // FireBrick
0x00DC143C, // Crimson
0x00FF6347, // Tomato
0x00FF7F50, // Coral
0x00Cd5C5C, // Indian Red
0x00F08080, // Light Coral
0x00E9967A, // Dark Salmon
0x00FA8072, // Salmon
0x00FFA07A, // Light Salmon
0x00FF4500, // Orange Red
0x00FF8C00, // Dark Orange
0x00FFA500, // Orange
0x00FFD700, // Gold
0x00B8860B, // Dark Golden Rod
0x00DAA520, // Golden Rod
0x00EEE8AA, // Pale Golden Rod
0x00BDB76B, // Dark Kharki
0x00F0E68C, // Khaki
0x00808000, // Olive
0x00FFFF00, // Yellow
0x009ACD32, // Yellow Green
0x00556B2F, // Dark Olive Green
0x006B8E23, // Olive Drab
0x007CFC00, // Lawn Green
0x55777777, // half black
0x00ADFF2F, // Green Yellow
0x00006400, // Dark Green
0x00008000, // Green
0x00228B22, // Forest Green
0x0000FF00, // Green/Lime
0x0032CD32, // Lime Green
0x0090EE90, // Light Green
0x0098FB98, // Pale Green
0x008FBC8F, // Dark See Green
0x0000FA9A, // Medium Spring Green
0x0000FF7F, // Spring Green
0x002E8B57, // Sea Green
0x0066CDAA, // Medium Aqua Marine
0x003CB371, // Medium Sea Green
0x0020B2AA, // Light Sea Green
0x002F4F4F, // Dark Slate Gray
0x00008080, // Teal
0x00008B8B, // Dark Cyan
0x0000FFFF, // Aqua/Cyan
0x00E0FFFF, // Light Cyan
0xFF000000, // Transparent/Clear
};

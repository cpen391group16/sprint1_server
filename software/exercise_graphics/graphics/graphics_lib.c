/*
 * graphics_lib.c
 *
 *  Created on: Jan 30, 2018
 *      Author: Mathew
 */

#include <stdio.h>
#include "graphics_lib.h"
#include "font_lib.h"
#include "../util/util.h"

void write_a_pixel(int x, int y, int Colour)
{
	if(x < 0 || x > XRES || y < 0 || y > YRES) {
		return;
	}
	WAIT_FOR_GRAPHICS;				// is graphics ready for new command

	GraphicsX1Reg = x;				// write coords to x1, y1
	GraphicsY1Reg = y;
	GraphicsColourReg = Colour;			// set pixel colour
	GraphicsCommandReg = PutAPixel;			// give graphics "write pixel" command
}

int read_a_pixel(int x, int y)
{
	if(x < 0 || x > XRES || y < 0 || y > YRES) {
		return -1;
	}
	WAIT_FOR_GRAPHICS;			// is graphics ready for new command

	GraphicsX1Reg = x;			// write coords to x1, y1
	GraphicsY1Reg = y;
	GraphicsCommandReg = GetAPixel;		// give graphics a "get pixel" command

	WAIT_FOR_GRAPHICS;			// is graphics done reading pixel
	return (int)(GraphicsColourReg) ;	// return the palette number (colour)
}

void program_palette(int PaletteNumber, int RGB)
{
    WAIT_FOR_GRAPHICS;
    GraphicsColourReg = PaletteNumber;
    GraphicsX1Reg = RGB >> 16   ;        // program red value in ls.8 bit of X1 reg
    GraphicsY1Reg = RGB ;                // program green and blue into ls 16 bit of Y1 reg
    GraphicsCommandReg = ProgramPaletteColour; // issue command
}

void load_colour_palette(ColourPalette p) {
	if(p == NORMAL) {
		int i;
		for(i = 0; i < 64; i++) {
			program_palette(i, ColourPalletteData[i]);
		}
	}else if(p == GRAYSCALE) {
		// Generate a grayscale palette by dividing the spectrum between black and white
		// into the number of spaces in the colorpalette
		int i;
		for(i = 0; i < 64; i++) {
			unsigned int temp = 0xFF / 64 * i;
			unsigned int intensity = (temp << 16) | (temp << 8) | temp;
			program_palette(i, intensity);
		}
	}
}

void h_line(int x1, int y1, int length, int Colour)
{
	if(y1 < 0 || y1 > YRES || length == 0) {
		return;
	}

	// Adjust the parameters to always be in the correct order, with length being a positive number
	int x, y, l;
	if(length < 0) {
		x = x1 + length;
		y = y1;
		l = abs(length);
	}else {
		x = x1;
		y = y1;
		l = length;
	}

	WAIT_FOR_GRAPHICS;			// is graphics ready for new command

	GraphicsX1Reg = x;			// write coords to x1, y1
	GraphicsY1Reg = y;
	GraphicsX2Reg = x + l;			// write coords to x1, y1
	GraphicsY2Reg = y;
	GraphicsColourReg = Colour;		// set pixel colour with a palette number
	GraphicsCommandReg = DrawHLine;		// give graphics a "HLine" command
}

void v_line(int x1, int y1, int length, int Colour)
{
	if(x1 < 0 || x1 > XRES || length == 0) {
		return;
	}

	// Adjust the parameters to always be in the correct order, with length being a positive number
	int x, y, l;
	if(length < 0) {
		x = x1;
		y = y1 + length;
		l = abs(length);
	}else {
		x = x1;
		y = y1;
		l = length;
	}

	WAIT_FOR_GRAPHICS;			// is graphics ready for new command

	GraphicsX1Reg = x;			// write coords to x1, y1
	GraphicsY1Reg = y;
	GraphicsX2Reg = x;			// write coords to x1, y1
	GraphicsY2Reg = y + l;
	GraphicsColourReg = Colour;		// set pixel colour with a palette number
	GraphicsCommandReg = DrawVLine;		// give graphics a "VLine" command
}

void line(int x1, int y1, int x2, int y2, int Colour)
{
	WAIT_FOR_GRAPHICS;			// is graphics ready for new command

	GraphicsX1Reg = x1;			// write coords to x1, y1
	GraphicsY1Reg = y1;
	GraphicsX2Reg = x2;			// write coords to x2, y2
	GraphicsY2Reg = y2;
	GraphicsColourReg = Colour;		// set pixel colour with a palette number
	GraphicsCommandReg = DrawLine;		// give graphics a "Draw Line" command
}

void RectHelper(int x1, int y1, int x2, int y2, int Colour, int fill) {
	WAIT_FOR_GRAPHICS;			// is graphics ready for new command

	GraphicsX1Reg = x1;			// write coords to x1, y1
	GraphicsY1Reg = y1;
	GraphicsX2Reg = x2;			// write coords to x2, y2
	GraphicsY2Reg = y2;
	GraphicsColourReg = Colour;		// set pixel colour with a palette number

	if(fill)
		GraphicsCommandReg = DrawFilledRect;
	else
		GraphicsCommandReg = DrawRect;
}

void rect(int x1, int y1, int width, int height, int Colour, int borderColour, int borderThickness, int fill) {
	rect2(x1, y1, x1 + width, y1 + height, Colour, borderColour, borderThickness, fill);
}

void rect2(int x1, int y1, int x2, int y2, int Colour, int borderColour, int borderThickness, int fill) {
	x1 = min(x1, x2);
	y1 = min(y1, y2);
	x2 = max(x1, x2);
	y2 = max(y1, y2);

	RectHelper(x1, y1, x2, y2, Colour, fill);

	if(borderThickness > 0) {
		RectHelper(x1 - borderThickness, y1 - borderThickness, x2 + borderThickness, y1, borderColour, 1);
		RectHelper(x1 - borderThickness, y2, x2 + borderThickness, y2 + borderThickness, borderColour, 1);
		RectHelper(x1 - borderThickness, y1, x1, y2, borderColour, 1);
		RectHelper(x2, y1, x2 + borderThickness, y2, borderColour, 1);
	}

	// Here we also support the ability to draw with a "negative" border. This is useful for internal button borders
	if(borderThickness < 0) {
		RectHelper(x1 - 1, y1, x2, y1 - borderThickness, borderColour, 1);
		RectHelper(x1 - 1, y2 + borderThickness, x2, y2, borderColour, 1);
		RectHelper(x1 - 1, y1, x1 - borderThickness, y2, borderColour, 1);
		RectHelper(x2 + borderThickness, y1, x2, y2, borderColour, 1);
	}
}

void fillBottomFlatTriangle(Point v1, Point v2, Point v3, int Colour)
{
  float invslope1 = (v2.x - v1.x) / (v2.y - v1.y);
  float invslope2 = (v3.x - v1.x) / (v3.y - v1.y);

  float curx1 = v1.x;
  float curx2 = v1.x;

  int scanlineY;
  for (scanlineY = v1.y; scanlineY <= v2.y; scanlineY++)
  {
    // Make use of the hardware-accelerated lines for filling
    h_line((int)curx1, scanlineY, (int)curx2 - (int)curx1, Colour);
    curx1 += invslope1;
    curx2 += invslope2;
  }
}

void fillTopFlatTriangle(Point v1, Point v2, Point v3, int Colour)
{
  float invslope1 = (v3.x - v1.x) / (v3.y - v1.y);
  float invslope2 = (v3.x - v2.x) / (v3.y - v2.y);

  float curx1 = v3.x;
  float curx2 = v3.x;

  int scanlineY;
  for (scanlineY = v3.y; scanlineY > v1.y; scanlineY--)
  {
	// Make use of the hardware-accelerated lines for filling
    h_line((int)curx1, scanlineY, (int)curx2 - (int)curx1, Colour);
    curx1 -= invslope1;
    curx2 -= invslope2;
  }
}

// This triangle drawing algorithm is from the following link
// http://www.sunshine2k.de/coding/java/TriangleRasterization/TriangleRasterization.html
void TriangleFill(Point p1, Point p2, Point p3, int Colour) {
	/* at first sort the three vertices by y-coordinate ascending so v1 is the topmost vertex on the screen */
	Point v3 = get_larger_y(get_larger_y(p1, p2), p3);
	Point v1 = get_smaller_y(get_smaller_y(p1, p2), p3);
	Point v2;

	if(!points_eq(p1, v1) && !points_eq(p1, v3))
		v2 = p1;
	else if(!points_eq(p2, v1) && !points_eq(p2, v3))
		v2 = p2;
	else
		v2 = p3;

	/* here we know that v1.y <= v2.y <= v3.y */
	/* check for trivial case of bottom-flat triangle */
	if (v2.y == v3.y) {
		fillBottomFlatTriangle(v1, v2, v3, Colour);
	}
	/* check for trivial case of top-flat triangle */
	else if (v1.y == v2.y) {
		fillTopFlatTriangle(v1, v2, v3, Colour);
	}
	else
	{
		/* general case - split the triangle in a topflat and bottom-flat one */
		Point v4 = {(int)(v1.x + ((float)(v2.y - v1.y) / (float)(v3.y - v1.y)) * (v3.x - v1.x)), v2.y};
		fillBottomFlatTriangle(v1, v2, v4, Colour);
		fillTopFlatTriangle(v2, v4, v3, Colour);
	}
}

void triangle_points(Point p1, Point p2, Point p3, int Colour, int fill) {
	if(fill) {
		TriangleFill(p1, p2, p3, Colour);
	}else {
		// If we don't need to fill the triangle, we can simply draw 3 lines
		line(p1.x, p1.y, p2.x, p2.y, Colour);
		line(p1.x, p1.y, p3.x, p3.y, Colour);
		line(p2.x, p2.y, p3.x, p3.y, Colour);
	}
}

void triangle(int x1, int y1, int x2, int y2, int x3, int y3, int Colour, int fill) {
	Point p1 = {x1, y1};
	Point p2 = {x2, y2};
	Point p3 = {x3, y3};
	triangle_points(p1, p2, p3, Colour, fill);
}

// function to draw all other 7 pixels
// present at symmetric position
void CircleOutlineHelper(int xc, int yc, int x, int y, int Colour)
{
	write_a_pixel(xc+x, yc+y, Colour);
	write_a_pixel(xc-x, yc+y, Colour);
	write_a_pixel(xc+x, yc-y, Colour);
	write_a_pixel(xc-x, yc-y, Colour);
	write_a_pixel(xc+y, yc+x, Colour);
	write_a_pixel(xc-y, yc+x, Colour);
	write_a_pixel(xc+y, yc-x, Colour);
	write_a_pixel(xc-y, yc-x, Colour);
}

// This circle-drawing algorithm is from the following link
// https://www.geeksforgeeks.org/bresenhams-circle-drawing-algorithm/
void CircleOutline(int xc, int yc, int r, int Colour) {
	int x = 0, y = r;
	int d = 3 - 2 * r;
	while (y >= x)
	{
		// for each pixel we will
		// draw all eight pixels
		CircleOutlineHelper(xc, yc, x, y, Colour);
		x++;

		// check for decision parameter
		// and correspondingly
		// update d, x, y
		if (d > 0)
		{
			y--;
			d = d + 4 * (x - y) + 10;
		}
		else
			d = d + 4 * x + 6;
		CircleOutlineHelper(xc, yc, x, y, Colour);
	}
}

void circle(int x, int y, int r, int Colour, int borderColour, int borderThickness, int fill) {
	int i;
	for(i = 1; i <= borderThickness; i++) {
		CircleOutline(x, y, r+i, borderColour);
	}

	CircleOutline(x, y, r, Colour);

	if(fill) {
		for(i = 0; i <= r; i++) {
			CircleOutline(x, y, r - i, Colour);
		}
	}
}

void clear(int Colour) {
	rect(0, 0, XRES, YRES, Colour, Colour, 0, 1);
}

void marker(Point p, char *text, int Colour){
	rect2(p.x-15, p.y-15,p.x, p.y, Colour, 0, 0, 1);
	draw_string(text, p.x, p.y-35, 0, 1, SMALL , 0);
}

void legend_marker(Point p, char *text, int Colour){
	rect2(p.x, p.y,p.x+15, p.y+15, Colour, 0, 0, 1);
	draw_string(text, p.x+20, p.y, 0, 1, SMALL , 0);
}

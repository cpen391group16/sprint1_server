/*
 * fontlib.c
 *
 *  Created on: Jan 30, 2018
 *      Author: Mathew
 */

#include "image_lib.h"
#include "graphics_lib.h"

void draw_image(int x, int y, Image img) {
	load_colour_palette(img.palette);

	register int px = 0, py = 0;
	register int index = 0;
	register unsigned char value = 0;
	for(px = 0; px < img.width; px++) {
		for(py = 0; py < img.height; py++) {
			index = img.width * py + px;
			value = img.img_array[index];
			write_a_pixel(x + px, y + py, value);
		}
	}
}

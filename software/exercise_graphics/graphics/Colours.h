/*
 * Colours.h
 *
 *  Created on: Mar 14, 2015
 *      Author: Paul
 */

// see --http://www.rapidtables.com/web/color/RGB_Color.htm for example colours

// This file creates symbolic names for the colours that you can specify in your program
// the actual RGB values are set up in the FPGA Rom, so the name BLACK for example, refer to colour
// palette number 0 whose 24 bit RGB value is 0x000000, WHITE is palette number 1 = RGB value 0xFFFFFF etc
//
// See the ColourPalette.c source file

#ifndef COLOURS_H_
#define COLOURS_H_

typedef enum {
	NORMAL,
	GRAYSCALE
} ColourPalette;

// create a set of symbolic constants representing colour name e.g. BLACK, WHITE, RED = {0, 1, 2...} etc and a new data type called Colour (see bottom)
typedef enum  {
BLACK,
WHITE,
RED,
LIME,
BLUE,
YELLOW,
CYAN,
MAGENTA,
SILVER,
GRAY,
MAROON,
OLIVE,
GREEN,
PURPLE,
TEAL,
NAVY,
DARKRED ,
BROWN,
FIREBRICK,
CRIMSON,
TOMATO,
CORAL,
INDIAN_RED,
LIGHT_CORAL,
DARK_SALMON,
SALMON,
LIGHT_SALMON,
ORANGE_RED,
DARK_ORANGE,
ORANGE,
GOLD,
DARK_GOLDEN_ROD,
GOLDEN_ROD,
PALE_GOLDEN_ROD,
DARK_KHAKI,
KHAKI,
OLIVE_REPEAT,
YELLOW_REPEAT,
YELLOW_GREEN,
DARK_OLIVE_GREEN,
OLIVE_DRAB,
LAWN_GREEN,
HALF_BLACK,
GREEN_YELLOW,
DARK_GREEN,
GREEN_REPEAT,
FOREST_GREEN,
LIME_REPEAT,
LIME_GREEN,
LIGHT_GREEN,
PALE_GREEN,
DARK_SEA_GREEN,
MEDIUM_SPRING_GREEN,
SPRING_GREEN,
SEA_GREEN,
MEDIUM_AQUA_MARINE,
MEDIUM_SEA_GREEN,
LIGHT_SEA_GREEN,
DARK_SLATE_GRAY,
TEAL_REPEAT,
DARK_CYAN,
AQUA,
LIGHT_CYAN,
TRANSPARENT
} Colour;

#endif /* COLOURS_H_ */

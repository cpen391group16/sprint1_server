/*
 * graphics_lib.h
 *
 *  Created on: Jan 30, 2018
 *      Author: Mathew
 */

#ifndef GRAPHICS_LIB_H_
#define GRAPHICS_LIB_H_

#include "../util/util.h"
#include "../util/geom.h"
#include "Colours.h"

// graphics registers all address begin with '8' so as to by pass data cache on NIOS
#define GraphicsCommandReg   		(*(volatile unsigned short int *)(0x84000000))
#define GraphicsStatusReg   		(*(volatile unsigned short int *)(0x84000000))
#define GraphicsX1Reg   			(*(volatile unsigned short int *)(0x84000002))
#define GraphicsY1Reg   			(*(volatile unsigned short int *)(0x84000004))
#define GraphicsX2Reg   			(*(volatile unsigned short int *)(0x84000006))
#define GraphicsY2Reg   			(*(volatile unsigned short int *)(0x84000008))
#define GraphicsColourReg   		(*(volatile unsigned short int *)(0x8400000E))
#define GraphicsBackGroundColourReg   	(*(volatile unsigned short int *)(0x84000010))

// The x and y resolution of the graphics controller
#define XRES 800
#define YRES 480

const extern unsigned int ColourPalletteData[];

/************************************************************************************************
** This macro pauses until the graphics chip status register indicates that it is idle
***********************************************************************************************/
#define WAIT_FOR_GRAPHICS		while((GraphicsStatusReg & 0x0001) != 0x0001);

// #defined constants representing values we write to the graphics 'command' register to get
// it to draw something. You will add more values as you add hardware to the graphics chip
// Note DrawHLine, DrawVLine and DrawLine at the moment do nothing - you will modify these
#define DrawHLine		1
#define DrawVLine		2
#define DrawLine		3
#define DrawRect		4
#define DrawFilledRect	5
#define	PutAPixel		0xA
#define	GetAPixel		0xB
#define	ProgramPaletteColour    0x10

/*******************************************************************************************
* This function writes a single pixel to the x,y coords specified using the specified colour
* Note colour is a byte and represents a palette number (0-255) not a 24 bit RGB value
********************************************************************************************/
void write_a_pixel(int x, int y, int Colour);

/*********************************************************************************************
* This function read a single pixel from the x,y coords specified and returns its colour
* Note returned colour is a byte and represents a palette number (0-255) not a 24 bit RGB value
*********************************************************************************************/
int read_a_pixel(int x, int y);

/**********************************************************************************
** subroutine to program a hardware (graphics chip) palette number with an RGB value
** e.g. ProgramPalette(RED, 0x00FF0000) ;
**
************************************************************************************/
void program_palette(int PaletteNumber, int RGB);
void load_colour_palette(ColourPalette p);

/*********************************************************************************************
This function draw a horizontal line, 1 pixel at a time starting at the x,y coords specified
*********************************************************************************************/
void h_line(int x1, int y1, int length, int Colour);

/*********************************************************************************************
This function draw a vertical line, 1 pixel at a time starting at the x,y coords specified
*********************************************************************************************/
void v_line(int x1, int y1, int length, int Colour);

/*********************************************************************************************
This function draw a line between x1, y1 and x2, y2
*********************************************************************************************/
void line(int x1, int y1, int x2, int y2, int Colour);

/*********************************************************************************************
Draws a rect with the top-left point at x1, y1
*********************************************************************************************/
void rect(int x1, int y1, int width, int height, int Colour, int borderColour, int borderThickness, int fill);
void rect2(int x1, int y1, int x2, int y2, int Colour, int borderColour, int borderThickness, int fill);

/*********************************************************************************************
Draws a triangle with vertices at the 3 points
*********************************************************************************************/
void triangle_points(Point p1, Point p2, Point p3, int Colour, int fill);
void triangle(int x1, int y1, int x2, int y2, int x3, int y3, int Colour, int fill);

/*********************************************************************************************
Draws a triangle with vertices at the 3 points
*********************************************************************************************/
void circle(int x, int y, int r, int Colour, int borderColour, int borderThickness, int fill);

/*********************************************************************************************
Clears the screen with the given colour
*********************************************************************************************/
void clear(int Colour);

/*********************************************************************************************
Draws marker on point p with the given colour
*********************************************************************************************/
void marker(Point p, char *text, int Colour);

/*********************************************************************************************
Draws a legend marker on point p with the given colour
*********************************************************************************************/
void legend_marker(Point p, char *text, int Colour);

#endif /* GRAPHICS_LIB_H_ */

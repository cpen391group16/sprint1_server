/*
 * lacd_touch.h
 *
 *  Created on: Jan 25, 2018
 *      Author: Mathew
 *      Author: Timothy
 */

#ifndef LCD_TOUCH_H_
#define LCD_TOUCH_H_

#include "../util/geom.h"

enum touch_status{NOCOMMAND, PRESSED, RELEASED};

typedef struct touch{
	enum touch_status status;
	Point point;
} touch;

/* Initialize the lcd touch device, and prepare it to send touch events */
void init_touch(void);


/* Returns a touch struct which contains coordinates & touch status
 *
 * Returns: touch struct containing:
 * status which is an enum containing {NOCOMMAND, PRESSED or RELEASED}
 * point which is a struct containing x,y values when status is PRESSED OR RELEASED
 */
touch touch_screen_status(void);

#endif /* LCD_TOUCH_H_ */

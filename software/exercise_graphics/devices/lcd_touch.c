/*
 * lcd_touch.c

 *
 *  Created on: Jan 25, 2018
 *      Author: Mathew
 */

#include <stdio.h>
#include "lcd_touch.h"
#include "../graphics/graphics_lib.h"

#define LCD_TOUCH_CONTROL (*(volatile unsigned char *)(0x84000230))
#define LCD_TOUCH_STATUS (*(volatile unsigned char *)(0x84000230))
#define LCD_TOUCH_TXDATA (*(volatile unsigned char *)(0x84000232))
#define LCD_TOUCH_RXDATA (*(volatile unsigned char *)(0x84000232))
#define LCD_TOUCH_BAUD (*(volatile unsigned char *)(0x84000234))

// The number of unique coordinates that can be returned by the lcd
#define TOUCH_RESOLUTION 4096

void init_touch(void)
{
	// set up 6850 Control Register to utilise a divide by 16 clock,
	// set RTS low, use 8 bits of data, no parity, 1 stop bit,
	// transmitter interrupt disabled
	LCD_TOUCH_CONTROL = 0x15;

	// program baud rate generator to use 9600 baud
	// The loew 3 bits must not be 001, 010, 011, 100
	LCD_TOUCH_BAUD = 0x05;

	// Send the byte command to enable touch
	LCD_TOUCH_TXDATA = 0x12;
}

// the following function polls the 6850 to determine if any character
// has been received. It doesn't wait for one, or read it, it simply tests
// to see if one is available to read
int is_data_ready_touch(void)
{
	// Test Rx bit in 6850 serial comms chip status register
	// if RX bit is set, return TRUE, otherwise return FALSE
	return 0x01 & LCD_TOUCH_STATUS;
}

char getbyte_lcd_touch( void )
{
	// Poll RX bit in 6850 status register and await for it to become '1'
	while (!is_data_ready_touch()) {};

	// Read the received character from 6850 RxData register.
	return LCD_TOUCH_RXDATA & 0xff;
}



/* Returns a touch struct which contains coordinates & touch status
 *
 * Returns: touch struct containing:
 * status which is an enum containing {NOCOMMAND, PRESSED or RELEASED}
 * point which is a struct containing x,y values when status is PRESSED OR RELEASED
 */
touch touch_screen_status(void){

	touch t;

	t.status = NOCOMMAND;

	if(is_data_ready_touch()) {

		/* Read first byte and check if it is a leading press/release byte */
		char type = getbyte_lcd_touch();

		if((type & 0xff) == 0x80) {
			t.status = RELEASED;
		}
		else if((type & 0xff) == 0x81) {
			t.status = PRESSED;
		}
		else{
			// Throw away byte if it is not the status header
			return t;
		}

		/* Get next four x,y bytes only if the previous byte was a touch/release byte*/
		char x1 = getbyte_lcd_touch();
		char x2 = getbyte_lcd_touch();
		char y1 = getbyte_lcd_touch();
		char y2 = getbyte_lcd_touch();

		/* Extract the coordinates */
		t.point.x = (int)((int)(x2 & 0b00011111) << 7) | (int)(x1 & 0b01111111);
		t.point.y = (int)((int)(y2 & 0b00011111) << 7) | (int)(y1 & 0b01111111);

		/* Map the coordinates to pixel values */
		t.point.x = (int) (XRES * t.point.x / TOUCH_RESOLUTION);
		t.point.y = (int) (YRES * t.point.y / TOUCH_RESOLUTION);

		//printf("%02hhX %02hhX %02hhX %02hhX %02hhX", type & 0xff, x1 & 0xff, x2 & 0xff, y1 & 0xff , y2 & 0xff);
		//printf("X: %d Y: %d", t.point.x, t.point.y);
		//printf(" Touch Status %d\n", t.status);
	}
	return t;
}

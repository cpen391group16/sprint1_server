/*
 * WiFi.c
 *
 * Contains impmentationsS for functions related to communicating with the ESP8266 through serial
 *
 * Author: Timothy Leung
 */

#include <stdio.h>
#include <stdlib.h>
#include "WiFi.h"
#include "String.h"


#define WIFI_Control (*(volatile unsigned char *)(0x84000240))
#define WIFI_Status (*(volatile unsigned char *)(0x84000240))
#define WIFI_TxData (*(volatile unsigned char *)(0x84000242))
#define WIFI_RxData (*(volatile unsigned char *)(0x84000242))
#define WIFI_Baud (*(volatile unsigned char *)(0x84000244))

/*
 * Initializes WiFi Serial UART coms with baudrate and other serial settings
 */
void init_wifi(void)
{

	// set up 6850 Control Register to utilise a divide by 16 clock,
	// set RTS low, use 8 bits of data, no parity, 1 stop bit,
	// transmitter interrupt disabled
	WIFI_Control = 0x15;

	// program baud rate generator to use 19.2k baud
	WIFI_Baud = 0x07;

}

int sendchar_wifi(int c)
{
	// Poll Tx bit in 6850 status register and await for it to become '1'
	while (!(0x02 & WIFI_Status)) {};

	// Write the character to the 6850 TxData register.
	WIFI_TxData = c;

	return c;
}

/* Block and get a single character from RX buffer
 *
 * This function is only used in this file. It does not have a prototype in WiFi.h
 * */
int getchar_wifi( void )
{
	// Poll RX bit in 6850 status register and await for it to become '1'
	while (!(0x01 & WIFI_Status)) {};

	// Read the received character from 6850 RxData register.
	return (int) WIFI_RxData;
}

/*
 * Check if data is ready in the RX buffer
 *
 * @returns 1 if data is ready to be read in the buffer 0 otherwise*/
int is_data_ready_wifi(void)
{
	// Test Rx bit in 6850 serial comms chip status register
	// if RX bit is set, return TRUE, otherwise return FALSE
	return 0x01 & WIFI_Status;
}

/*
 * Use this function upon initialization. It attempts to read a character from buffer if it exists. Returns if buffer is empty
 *
 */
void clear_buffer(void)
{
	if(is_data_ready_wifi()){
		getchar_wifi();
	}
}

/*
 * Concatenates header and payload and sends the resulting string through the WiFi TX
 *
 * @param header message, goes first
 * @param payload message, goes second
 */
void sendmessage_wifi(char* header, char* payload)
{
	int packet_size = strlen(header) + strlen(payload);
	char* packet[packet_size];
	sprintf(packet,"%s%s",header,payload);
	sendstring_wifi(packet);
}

/*
 * Sends header, payload terminted by '\r'. Waits for a '\r' terminted response from serial RX and places it it response
 *
 * @param header for string payload for serial TX
 * @param payload for serial TX
 * @param response the '/r' response from the ESP8266 will be placed here
 */
void sendandgetresponse_wifi(char* header, char* payload, char* response)
{
	/* Send string to TX */
	sendjson_wifi(header,payload);

	int wifi_msg_index = 0;

	/* Block and read '\r' terminated response into response array */
	while(1){
		response[wifi_msg_index] = getchar_wifi();

		if(response[wifi_msg_index] == '\r'){
			//printf("\nEXIT: %d %c \n",response[wifi_msg_index],response[wifi_msg_index]);
			response[wifi_msg_index] = '\0';
			break;

		}
		wifi_msg_index++;
	}

	printf("WiFi RX: %s\n",response);
	return;
}

/*
 * Sends a string through the WiFi TX
 *
 * @param message string to send
 */
void sendstring_wifi(char* message){
	printf("WiFi TX: %s\n",message);
	int i = 0;
	while(message[i] != '\0'){
		sendchar_wifi(message[i++]);
	}
	sendchar_wifi('\r');
}

/*
 * Processes a single character if ready and places it into the WiFi Buffer and increases wifiMsgIndex.
 * If a '\r' is recieved, wifiMsgReady is set to 1.
 *
 * @param wifi_buffer Pointer to first char of char array to store next character recieved
 * @param wifi_msg_ready signals when a '\r' is recieved and the contents in wifi_buffer can be read
 * @param wifi_msg_index the index of wifi_buffer
 */
void process_wifi_buffer(char* wifi_buffer, int* wifi_msg_index, int* wifi_msg_ready)
{
	if(is_data_ready_wifi()){
		wifi_buffer[*wifi_msg_index] = getchar_wifi();

		if(wifi_buffer[*wifi_msg_index] == '\r'){
			/* Response has been completed. Mark as ready for processing*/
			(*wifi_msg_ready) = 1;
			wifi_buffer[*wifi_msg_index] = '\0';
		}
		else if(wifi_buffer[*wifi_msg_index] == '\n' || wifi_buffer[*wifi_msg_index] == -1){
			/* Discard and ignore these characters in the response */
			return;
		}
		else{
			/* Succesfully read one character into wifi_buffer*/
			(*wifi_msg_index)++;
		}
	}
}


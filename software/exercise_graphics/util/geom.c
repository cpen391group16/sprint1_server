/*
 * util.c
 *
 *  Created on: Jan 30, 2018
 *      Author: Mathew
 */

#include "geom.h"
#include "util.h"

Point get_larger_y(Point a, Point b) {
	if(a.y > b.y) {
		return a;
	}

	return b;
}

Point get_smaller_y(Point a, Point b) {
	if(a.y < b.y) {
		return a;
	}

	return b;
}

int points_eq(Point a, Point b) {
	return a.x == b.x && a.y == b.y;
}


int inside_bounds(Point p, Bounds b) {
	return p.x >= min(b.x1, b.x2) &&
		   p.x <= max(b.x1, b.x2) &&
		   p.y >= min(b.y1, b.y2) &&
		   p.y <= max(b.y1, b.y2);
}

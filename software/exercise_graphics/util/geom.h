/*
 * util.h
 *
 *  Created on: Jan 30, 2018
 *      Author: Mathew
 */

#ifndef POINT_H_
#define POINT_H_

// Defines a point in 2D space, with an x and y coordinate
typedef struct Point {
	int x;
	int y;
} Point;

// Returns the point with the larger y-coordinate
Point get_larger_y(Point a, Point b);

// Returns the point with the smaller y-coordinate
Point get_smaller_y(Point a, Point b);

// Returns 1 if the points are equal, and 0 otherwise
int points_eq(Point a, Point b);

// Defines an arbitrary set of bounds/boundaries
typedef struct Bounds {
	int x1, x2, y1, y2;
} Bounds;

// Returns 1 if the given point is contained by the bounds, and 0 otherwise
int inside_bounds(Point p, Bounds b);

#endif /* UTIL_H_ */

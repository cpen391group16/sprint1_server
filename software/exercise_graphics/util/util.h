/*
 * util.h
 *
 *  Created on: Jan 30, 2018
 *      Author: Mathew
 */
#include "geom.h"

#ifndef UTIL_H_
#define UTIL_H_

// Returns the larger of the two integer values
int max(int a, int b);

// Returns the smaller of the two integer values
int min(int a, int b);

// Returns the absolute value of the given integer
int abs(int a);

/*
 * Converts lat and lon string to an x y coordinate. This function is only useful for the UBC map in our implementation
 *
 * char *lat
 * char *long
 *
 * returns: Point
 */
Point convert_coord_to_x_y(char* lat, char* lon);


/*
 * Add trailing spaces to such that the new string length is 14
 *
 * string
 *
 */
void add_trailing_spaces_to_14(char* string);


void undo_trailing_spaces(char* string);

#endif /* UTIL_H_ */

-- This information is used by the Wi-Fi dongle to make a wireless connection to the router in the Lab
-- or if you are using another router e.g. at home, change ID and Password appropriately
SSID = "CPEN391"
SSID_PASSWORD = "group16wifi"

-- HTTP SERVER IP --
SERVER_IP = "128.189.94.104"

-- configure ESP as a station
wifi.setmode(wifi.STATION)
wifi.sta.config(SSID,SSID_PASSWORD)
wifi.sta.autoconnect(1)

-- Configure Serial Comms 
uart.setup(0, 19200, 8, uart.PARITY_NONE, uart.STOPBITS_1, 0)

uart.on("data", "\r",
  -- Handle '\r' terminted request
  function(data)
    ip = wifi.sta.getip()
    -- Response with WAIT if not connected to WiFi
    if(ip==nil) then
      uart.write(0,"WAIT\r")
      return
    end   

    if data=="quit\r" then
      uart.on("data") -- unregister callback function
    elseif string.sub(data,1,3) == "gps" then
      -- PUT /guests/id/ (lat/lon)
      -- Used to update a given Guest's GPS information 
     http.put(string.format("http://%s:3001/guests/%s/",SERVER_IP,string.sub(data,4,4)),
          'Content-Type: application/json\r\n',
          string.sub(data,5),
          function(code, data)
            if (code < 0) then
              uart.write(0,"HTTP request failed")
            else
              --uart.write(0,"Result: ")
              --uart.write(0, code)
              uart.write(0,"gst")
              uart.write(0, data)
            end
            uart.write(0,'\r')
          end)
     elseif string.sub(data,1,3) == "gst" then
        -- GET /guests/id/
        -- Used to retrieve a guest's information including their ticket information
           http.get(string.format("http://%s:3001/guests/%s/",SERVER_IP,string.sub(data,4,4)),
            'Accept: application/json\r\n',
             function(code, data)
                if (code < 0) then
                  uart.write(0,"HTTP request failed")
                else
                  uart.write(0,"gst")
                  uart.write(0, data)
                end
                uart.write(0,'\r')
              end)
     elseif string.sub(data,1,3) == "loc" then
    -- GET /guests/id/gps
    -- USed to retrieve a guest's location information only
       http.get(string.format("http://%s:3001/guests/%s/gps",SERVER_IP,string.sub(data,4,4)),
        'Content-Type: application/json\r\n',
         function(code, data)
            if (code < 0) then
              uart.write(0,"HTTP request failed")
            else
              uart.write(0,"loc")
              uart.write(0, data)
            end
            uart.write(0,'\r')
          end)          
    elseif string.sub(data,1,4) == "ride" then
        -- GET /rides/
        -- USed to retrieve all ride information including wait times in the park
         http.get(string.format("http://%s:3001/rides/",SERVER_IP),
          'Content-Type: application/json\r\n',
          function(code, data)
            if (code < 0) then
              uart.write(0,"HTTP request failed")
            else
              uart.write(0,"rides")
              uart.write(0, data)
            end
            uart.write(0,'\r')
          end)
    elseif string.sub(data,1,3) == "tic" then
      -- POST /tickets/
      -- Used to generate a new ticket for a given guest and ride
        http.post(string.format("http://%s:3001/tickets/",SERVER_IP),
        'Content-Type: application/json\r\n',
        string.sub(data,4),
        function(code, data)
          if (code < 0) then
            uart.write(0,"HTTP request failed")
          else
            uart.write(0,"rides")
            uart.write(0, data)
          end
          uart.write(0,'\r')
        end)      
    end      
  end
,0)

-- PUSH NOTIFICATIONS --

-- Create a server object with 30 second timeout
srv = net.createServer(net.TCP, 30)

-- server listen on 80, if any HTTP request is recieved, send wake command through serial
srv:listen(80,function(conn)
    conn:on("receive", function(conn, payload)
        --print(payload) -- Print data from browser to serial terminal
    
        uart.write(0,'wake')
        uart.write(0,'\r')

        srv:send(table.concat ({
            "HTTP/1.1 200 OK",
            "Content-Type: application/json",
            "Content-length: 4" .. #body,
            "",
            body
          }, "\r\n"))
        conn:on("sent", function(conn) conn:close() end)
    end)
end)

    
